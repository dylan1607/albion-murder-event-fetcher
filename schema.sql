-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: albion
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `2v2_hg_battle`
--

DROP TABLE IF EXISTS `2v2_hg_battle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `2v2_hg_battle` (
  `battle_id` int unsigned NOT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `winner_1` varchar(25) DEFAULT NULL,
  `winner_2` varchar(25) DEFAULT NULL,
  `loser_1` varchar(25) DEFAULT NULL,
  `loser_2` varchar(25) DEFAULT NULL,
  `winner_1_loadout` binary(32) DEFAULT NULL,
  `winner_2_loadout` binary(32) DEFAULT NULL,
  `loser_1_loadout` binary(32) DEFAULT NULL,
  `loser_2_loadout` binary(32) DEFAULT NULL,
  PRIMARY KEY (`battle_id`),
  KEY `winner_1` (`winner_1`),
  KEY `winner_2` (`winner_2`),
  KEY `loser_1` (`loser_1`),
  KEY `loser_2` (`loser_2`),
  KEY `2v2_battle_hg_time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `build_stats`
--

DROP TABLE IF EXISTS `build_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `build_stats` (
  `stat_name` varchar(35) NOT NULL,
  `main_hand_item` varchar(40) NOT NULL,
  `off_hand_item` varchar(40) NOT NULL,
  `head_item` varchar(40) NOT NULL,
  `body_item` varchar(40) NOT NULL,
  `shoe_item` varchar(40) NOT NULL,
  `cape_item` varchar(40) NOT NULL,
  `time_added` timestamp NULL DEFAULT NULL,
  `usages` int unsigned NOT NULL,
  `avg_item_power` smallint unsigned NOT NULL,
  `kill_fame` int unsigned NOT NULL,
  `death_fame` int unsigned NOT NULL,
  `kills` int unsigned NOT NULL,
  `assists` int unsigned NOT NULL,
  `deaths` int unsigned NOT NULL,
  `fame_ratio` decimal(10,2) unsigned DEFAULT NULL,
  `win_rate` decimal(5,4) unsigned DEFAULT NULL,
  PRIMARY KEY (`stat_name`,`main_hand_item`,`off_hand_item`,`head_item`,`body_item`,`shoe_item`,`cape_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elo_1v1`
--

DROP TABLE IF EXISTS `elo_1v1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `elo_1v1` (
  `name` varchar(25) NOT NULL,
  `rating` smallint unsigned DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `rank` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elo_slayer_1v1`
--

DROP TABLE IF EXISTS `elo_slayer_1v1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `elo_slayer_1v1` (
  `name` varchar(25) NOT NULL,
  `rating` smallint unsigned DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `rank` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elo_slayer_1v1_history`
--

DROP TABLE IF EXISTS `elo_slayer_1v1_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `elo_slayer_1v1_history` (
  `season` varchar(20) NOT NULL,
  `name` varchar(25) NOT NULL,
  `rating` smallint unsigned NOT NULL,
  `rank` int unsigned NOT NULL,
  `weapon` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`season`,`name`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elo_stalker_1v1`
--

DROP TABLE IF EXISTS `elo_stalker_1v1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `elo_stalker_1v1` (
  `name` varchar(25) NOT NULL,
  `rating` smallint unsigned DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `rank` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elo_stalker_1v1_history`
--

DROP TABLE IF EXISTS `elo_stalker_1v1_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `elo_stalker_1v1_history` (
  `season` varchar(20) NOT NULL,
  `name` varchar(25) NOT NULL,
  `rating` smallint unsigned NOT NULL,
  `rank` int unsigned NOT NULL,
  `weapon` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`season`,`name`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elo_transaction_1v1`
--

DROP TABLE IF EXISTS `elo_transaction_1v1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `elo_transaction_1v1` (
  `winner_name` varchar(25) DEFAULT NULL,
  `loser_name` varchar(25) DEFAULT NULL,
  `event_id` int unsigned NOT NULL,
  `points_awarded` smallint unsigned DEFAULT NULL,
  `points_lost` smallint unsigned DEFAULT NULL,
  `is_slayer` tinyint unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`event_id`),
  KEY `winner_name` (`winner_name`),
  KEY `loser_name` (`loser_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `event_id` int unsigned NOT NULL,
  `time` timestamp NOT NULL,
  `version` tinyint DEFAULT NULL,
  `total_kill_fame` int DEFAULT NULL,
  `participant_count` tinyint DEFAULT NULL,
  `party_size` tinyint unsigned NOT NULL DEFAULT '0',
  `battle_id` int unsigned DEFAULT NULL,
  PRIMARY KEY (`event_id`,`time`),
  KEY `time` (`time`),
  KEY `idx_events_battle_id` (`battle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
/*!50100 PARTITION BY RANGE (unix_timestamp(`time`))
(PARTITION p2021q1 VALUES LESS THAN (1609459200) ENGINE = InnoDB,
 PARTITION p2021q2 VALUES LESS THAN (1617235200) ENGINE = InnoDB,
 PARTITION p2021q3 VALUES LESS THAN (1625097600) ENGINE = InnoDB,
 PARTITION p2021q4 VALUES LESS THAN (1633046400) ENGINE = InnoDB,
 PARTITION p2022q1 VALUES LESS THAN (1640995200) ENGINE = InnoDB,
 PARTITION p2022q2 VALUES LESS THAN (1648771200) ENGINE = InnoDB,
 PARTITION p2022q3 VALUES LESS THAN (1656633600) ENGINE = InnoDB,
 PARTITION p2022q4 VALUES LESS THAN (1664582400) ENGINE = InnoDB,
 PARTITION p2023q1 VALUES LESS THAN (1672531200) ENGINE = InnoDB,
 PARTITION p2023q2 VALUES LESS THAN (1680307200) ENGINE = InnoDB,
 PARTITION p2023q3 VALUES LESS THAN (1688169600) ENGINE = InnoDB,
 PARTITION p2023q4 VALUES LESS THAN (1696118400) ENGINE = InnoDB,
 PARTITION p2024q1 VALUES LESS THAN (1704067200) ENGINE = InnoDB,
 PARTITION p2024q2 VALUES LESS THAN (1711929600) ENGINE = InnoDB,
 PARTITION p2024q3 VALUES LESS THAN (1719792000) ENGINE = InnoDB,
 PARTITION p2024q4 VALUES LESS THAN (1727740800) ENGINE = InnoDB,
 PARTITION pdump VALUES LESS THAN MAXVALUE ENGINE = InnoDB) */;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `killers`
--

DROP TABLE IF EXISTS `killers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `killers` (
  `is_primary` tinyint unsigned DEFAULT NULL,
  `event_id` int unsigned NOT NULL,
  `name` varchar(25) NOT NULL,
  `kill_fame` int DEFAULT NULL,
  `damage_done` mediumint unsigned DEFAULT NULL,
  `healing_done` mediumint unsigned DEFAULT NULL,
  `item_power` smallint unsigned DEFAULT NULL,
  `guild_name` varchar(35) DEFAULT NULL,
  `alliance_name` varchar(35) DEFAULT NULL,
  `loadout` binary(32) DEFAULT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`,`event_id`,`added_on`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
/*!50100 PARTITION BY RANGE (unix_timestamp(`added_on`))
(PARTITION p2021q1 VALUES LESS THAN (1609459200) ENGINE = InnoDB,
 PARTITION p2021q2 VALUES LESS THAN (1617235200) ENGINE = InnoDB,
 PARTITION p2021q3 VALUES LESS THAN (1625097600) ENGINE = InnoDB,
 PARTITION p2021q4 VALUES LESS THAN (1633046400) ENGINE = InnoDB,
 PARTITION p2022q1 VALUES LESS THAN (1640995200) ENGINE = InnoDB,
 PARTITION p2022q2 VALUES LESS THAN (1648771200) ENGINE = InnoDB,
 PARTITION p2022q3 VALUES LESS THAN (1656633600) ENGINE = InnoDB,
 PARTITION p2022q4 VALUES LESS THAN (1664582400) ENGINE = InnoDB,
 PARTITION p2023q1 VALUES LESS THAN (1672531200) ENGINE = InnoDB,
 PARTITION p2023q2 VALUES LESS THAN (1680307200) ENGINE = InnoDB,
 PARTITION p2023q3 VALUES LESS THAN (1688169600) ENGINE = InnoDB,
 PARTITION p2023q4 VALUES LESS THAN (1696118400) ENGINE = InnoDB,
 PARTITION p2024q1 VALUES LESS THAN (1704067200) ENGINE = InnoDB,
 PARTITION p2024q2 VALUES LESS THAN (1711929600) ENGINE = InnoDB,
 PARTITION p2024q3 VALUES LESS THAN (1719792000) ENGINE = InnoDB,
 PARTITION p2024q4 VALUES LESS THAN (1727740800) ENGINE = InnoDB,
 PARTITION pdump VALUES LESS THAN MAXVALUE ENGINE = InnoDB) */;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `loadouts`
--

DROP TABLE IF EXISTS `loadouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loadouts` (
  `main_hand_item` varchar(40) DEFAULT NULL,
  `main_hand_tier` tinyint DEFAULT NULL,
  `main_hand_enchant` tinyint DEFAULT NULL,
  `main_hand_quality` tinyint DEFAULT NULL,
  `off_hand_item` varchar(40) DEFAULT NULL,
  `off_hand_tier` tinyint DEFAULT NULL,
  `off_hand_enchant` tinyint DEFAULT NULL,
  `off_hand_quality` tinyint DEFAULT NULL,
  `head_item` varchar(40) DEFAULT NULL,
  `head_tier` tinyint DEFAULT NULL,
  `head_enchant` tinyint DEFAULT NULL,
  `head_quality` tinyint DEFAULT NULL,
  `body_item` varchar(40) DEFAULT NULL,
  `body_tier` tinyint DEFAULT NULL,
  `body_enchant` tinyint DEFAULT NULL,
  `body_quality` tinyint DEFAULT NULL,
  `shoe_item` varchar(40) DEFAULT NULL,
  `shoe_tier` tinyint DEFAULT NULL,
  `shoe_enchant` tinyint DEFAULT NULL,
  `shoe_quality` tinyint DEFAULT NULL,
  `bag_item` varchar(40) DEFAULT NULL,
  `bag_tier` tinyint DEFAULT NULL,
  `bag_enchant` tinyint DEFAULT NULL,
  `bag_quality` tinyint DEFAULT NULL,
  `cape_item` varchar(40) DEFAULT NULL,
  `cape_tier` tinyint DEFAULT NULL,
  `cape_enchant` tinyint DEFAULT NULL,
  `cape_quality` tinyint DEFAULT NULL,
  `mount_item` varchar(40) DEFAULT NULL,
  `mount_tier` tinyint DEFAULT NULL,
  `mount_quality` tinyint DEFAULT NULL,
  `id` binary(32) NOT NULL,
  `food_item` varchar(40) NOT NULL DEFAULT '',
  `food_tier` tinyint NOT NULL DEFAULT '0',
  `food_enchant` tinyint NOT NULL DEFAULT '0',
  `potion_item` varchar(40) NOT NULL DEFAULT '',
  `potion_tier` tinyint NOT NULL DEFAULT '0',
  `potion_enchant` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `main_hand_item` (`main_hand_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `player_fav_weapon`
--

DROP TABLE IF EXISTS `player_fav_weapon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player_fav_weapon` (
  `name` varchar(25) NOT NULL,
  `weapon` varchar(40) NOT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `player_fav_weapon_1v1`
--

DROP TABLE IF EXISTS `player_fav_weapon_1v1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `player_fav_weapon_1v1` (
  `name` varchar(25) NOT NULL,
  `weapon` varchar(40) NOT NULL,
  `last_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `season_1v1`
--

DROP TABLE IF EXISTS `season_1v1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `season_1v1` (
  `season` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `start_date` timestamp NOT NULL,
  `end_date` timestamp NOT NULL,
  `is_archived` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`season`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitch_users`
--

DROP TABLE IF EXISTS `twitch_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `twitch_users` (
  `name` varchar(25) NOT NULL,
  `twitch_user_id` int NOT NULL DEFAULT '0',
  `twitch_name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `twitch_vods`
--

DROP TABLE IF EXISTS `twitch_vods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `twitch_vods` (
  `event_id` int unsigned NOT NULL,
  `name` varchar(25) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `victims`
--

DROP TABLE IF EXISTS `victims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `victims` (
  `event_id` int unsigned NOT NULL,
  `name` varchar(25) NOT NULL,
  `item_power` smallint unsigned DEFAULT NULL,
  `guild_name` varchar(35) DEFAULT NULL,
  `alliance_name` varchar(35) DEFAULT NULL,
  `loadout` binary(32) DEFAULT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`name`,`event_id`,`added_on`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
/*!50100 PARTITION BY RANGE (unix_timestamp(`added_on`))
(PARTITION p2021q1 VALUES LESS THAN (1609459200) ENGINE = InnoDB,
 PARTITION p2021q2 VALUES LESS THAN (1617235200) ENGINE = InnoDB,
 PARTITION p2021q3 VALUES LESS THAN (1625097600) ENGINE = InnoDB,
 PARTITION p2021q4 VALUES LESS THAN (1633046400) ENGINE = InnoDB,
 PARTITION p2022q1 VALUES LESS THAN (1640995200) ENGINE = InnoDB,
 PARTITION p2022q2 VALUES LESS THAN (1648771200) ENGINE = InnoDB,
 PARTITION p2022q3 VALUES LESS THAN (1656633600) ENGINE = InnoDB,
 PARTITION p2022q4 VALUES LESS THAN (1664582400) ENGINE = InnoDB,
 PARTITION p2023q1 VALUES LESS THAN (1672531200) ENGINE = InnoDB,
 PARTITION p2023q2 VALUES LESS THAN (1680307200) ENGINE = InnoDB,
 PARTITION p2023q3 VALUES LESS THAN (1688169600) ENGINE = InnoDB,
 PARTITION p2023q4 VALUES LESS THAN (1696118400) ENGINE = InnoDB,
 PARTITION p2024q1 VALUES LESS THAN (1704067200) ENGINE = InnoDB,
 PARTITION p2024q2 VALUES LESS THAN (1711929600) ENGINE = InnoDB,
 PARTITION p2024q3 VALUES LESS THAN (1719792000) ENGINE = InnoDB,
 PARTITION p2024q4 VALUES LESS THAN (1727740800) ENGINE = InnoDB,
 PARTITION pdump VALUES LESS THAN MAXVALUE ENGINE = InnoDB) */;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-27  1:18:25

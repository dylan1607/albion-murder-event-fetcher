package rnkr

import (
	"testing"
)

func TestNewRating_UncertaintySquaredIsCalculated(t *testing.T) {
	// Arrange
	uncertainty := 4.0

	// Act
	r := NewRating("a", 25, uncertainty)

	// Assert
	if uncertainty*uncertainty != r.UncertaintySquared {
		t.Fatalf("Expected rating.UncertaintySquared to be %f * %f, got %f", uncertainty, uncertainty, r.UncertaintySquared)
	}
}

func TestRating_RatingIsSkillMinusThreeTimesSigma(t *testing.T) {
	// Arrange
	rating := NewRating("a", 25, 5)

	// Act
	ratingVal := rating.Rating()

	// Assert
	if rating.EstimatedSkill-3*rating.Uncertainty != ratingVal {
		t.Fatalf("Expected rating %f %f", rating.EstimatedSkill-3*rating.Uncertainty, ratingVal)
	}
}

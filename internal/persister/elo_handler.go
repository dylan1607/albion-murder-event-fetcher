package persister

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"strings"

	"albion-event-fetcher/internal/elo"
	"albion-event-fetcher/internal/models"

	"github.com/jmoiron/sqlx"

	"github.com/getsentry/sentry-go"
)

const (
	// Starting elo given to each player at the start of a season
	initialElo = 1000

	// IP at which the slayer softcap beings
	slayerSoftCapStartIP = 1300.0

	// The magnitude of the slayer softcap
	slayerSoftCapAmount = 0.5

	// The maximum amount of IP bonus from mastery + spec
	// (2 * 120)   +   (0.2 * 120 * 6)   +   (0.1 * 120)   +   (0.2 * 100)
	// Primary Spec    Aux Spec              Ava Aux Spec      Mastery
	maxMasteryBonus = 416.0

	// The maximum IP deviation between the IP model and the actual IP before
	// an event is disqualified from counting as a slayer CD.
	//
	// Slayer has a 50% IP cap starting at 1300. We can use this information to
	// make an educated guess if a 1v1 was actually open world.
	IPModelDeviationThreshold = 250.0
)

var qualityIPMap = map[int]float64{
	0: 0.0,
	1: 20.0,
	2: 40.0,
	3: 60.0,
	4: 100.0,
}

var masteryModifierMap = map[int]float64{
	1: 0.00,
	2: 0.00,
	3: 0.00,
	4: 0.00,
	5: 0.05,
	6: 0.10,
	7: 0.15,
	8: 0.20,
}

var artifactBaseBonusMap = map[string]float64{
	"KEEPER": 25.0,
	"HELL":   50.0,
	"UNDEAD": 75.0,
	"AVALON": 100.0,
}

type eloHandler struct {
	db *sqlx.DB
}

func EloHandler() *eloHandler {
	handler := &eloHandler{}

	return handler
}

func (h *eloHandler) setup(db *sqlx.DB) {
	h.db = db
}

func (h *eloHandler) handleEvents(events []*models.Event) {
	for _, event := range events {
		h.handleEvent(event)
	}
}

func (h *eloHandler) handleEvent(event *models.Event) {
	isSlayer := is1v1SlayerEvent(event)

	if !is1v1StalkerEvent(event) && !isSlayer {
		return
	}

	winnerRating, err := h.getPlayerRating(event.Killer.Name, isSlayer)
	if err != nil {
		log.Println(err)
		sentry.CaptureException(fmt.Errorf("Failed to fetch winner elo for %s: %v\n", event.Killer.Name, err))
		return
	}
	loserRating, err := h.getPlayerRating(event.Victim.Name, isSlayer)
	if err != nil {
		log.Println(err)
		sentry.CaptureException(fmt.Errorf("Failed to fetch loser elo for %s: %v\n", event.Victim.Name, err))
		return
	}

	winnerPts, loserPts := elo.CalcPoints(
		winnerRating,
		loserRating,
		int(event.Killer.AverageItemPower),
		int(event.Victim.AverageItemPower),
	)

	err = h.updateElo(
		event.Killer.Name,
		event.Victim.Name,
		winnerRating,
		loserRating,
		winnerPts,
		loserPts,
		event.EventID,
		isSlayer,
	)

	if err != nil {
		log.Println(err)
		sentry.CaptureException(fmt.Errorf("Failed to write elo transaction for event %d: %v\n", event.EventID, err))
	}
}

func (h *eloHandler) getPlayerRating(name string, isSlayer bool) (int, error) {
	tableName := getEloTable(isSlayer)
	query := fmt.Sprintf("select rating from %s where name = ?", tableName)
	row := h.db.QueryRow(query, name)
	var rating int
	err := row.Scan(&rating)
	if err == sql.ErrNoRows {
		return initialElo, nil
	} else if err != nil {
		return 0, err
	}

	return rating, nil
}

func (h *eloHandler) updateElo(
	winnerName,
	loserName string,
	winnerRating,
	loserRating,
	winnerPts,
	loserPts,
	eventId int,
	isSlayer bool,
) error {
	tx, err := h.db.Begin()
	if err != nil {
		return err
	}

	newWinnerRating := winnerRating + winnerPts
	newLoserRating := loserRating - loserPts

	tableName := getEloTable(isSlayer)

	_, err = tx.Exec(fmt.Sprintf(
		"INSERT INTO %s (name, rating) VALUES (?, ?) ON DUPLICATE KEY UPDATE rating = ?, last_update= now()",
		tableName,
	), winnerName, newWinnerRating, newWinnerRating)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Exec(fmt.Sprintf(
		"INSERT INTO %s (name, rating) VALUES (?, ?) ON DUPLICATE KEY UPDATE rating = ?, last_update = now()",
		tableName,
	), loserName, newLoserRating, newLoserRating)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Exec(
		"INSERT INTO elo_transaction_1v1 (winner_name, loser_name, event_id, points_awarded, points_lost, is_slayer) VALUES (?, ?, ?, ?, ?, ?)",
		winnerName,
		loserName,
		eventId,
		winnerPts,
		loserPts,
		isSlayer,
	)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func getEloTable(isSlayer bool) string {
	if isSlayer {
		return "elo_slayer_1v1"
	}

	return "elo_stalker_1v1"
}

func is1v1StalkerEvent(event *models.Event) bool {
	isStalker := event.Killer.AverageItemPower > 900 && event.Killer.AverageItemPower < 1100 && event.Victim.AverageItemPower > 900 && event.Victim.AverageItemPower < 1100

	return event.GroupMemberCount == 1 && event.NumberOfParticipants == 1 && isStalker
}

func is1v1SlayerEvent(event *models.Event) bool {
	if event.Killer.AverageItemPower < 1180 || event.Victim.AverageItemPower < 1180 {
		return false // IP is too low for slayer
	}

	if event.GroupMemberCount != 1 || event.NumberOfParticipants != 1 {
		return false // Event is not a 1v1
	}

	expectedKillerIP := applySlayerSoftcap(maxAverageOpenWorldIP(event.Killer.Equipment))
	if event.Killer.AverageItemPower > expectedKillerIP {
		return false // Killer gear+average IP indicates open world event
	}

	expectedVictimIP := applySlayerSoftcap(maxAverageOpenWorldIP(event.Victim.Equipment))
	if event.Victim.AverageItemPower > expectedVictimIP {
		return false // Victim gear+average IP indicates open world event
	}

	return true
}

func applySlayerSoftcap(averageIP float64) float64 {
	if averageIP <= slayerSoftCapStartIP {
		return averageIP
	}

	penalizedIP := averageIP - slayerSoftCapStartIP

	return slayerSoftCapStartIP + (penalizedIP * slayerSoftCapAmount)
}

func maxAverageOpenWorldIP(eq models.PlayerEquipment) float64 {
	headIP := expectedAverageEquipmentIP(eq.Head, maxMasteryBonus, true)
	chestIP := expectedAverageEquipmentIP(eq.Armor, maxMasteryBonus, true)
	bootIP := expectedAverageEquipmentIP(eq.Shoes, maxMasteryBonus, true)
	mainHandIP := expectedAverageEquipmentIP(eq.MainHand, maxMasteryBonus, true)
	offHandIP := expectedAverageEquipmentIP(eq.OffHand, maxMasteryBonus, true)
	capeIP := expectedAverageEquipmentIP(eq.Cape, 0.0, true)

	// Account for 2 handed weapons
	if eq.OffHand.Type == "" && strings.Contains(eq.MainHand.Type, "_2H_") {
		offHandIP = mainHandIP
	}

	return (headIP + chestIP + bootIP + mainHandIP + offHandIP + capeIP) / 6.0
}

func expectedAverageEquipmentIP(eq models.Equipment, specIP float64, overcharge bool) float64 {
	if eq.Type == "" {
		return 0.0
	}

	baseIP := getBaseIPForItemType(eq.Type)
	overchargeIP := 0.0
    if (overcharge) {
        overchargeIP = 100.0
    }
	tier, enchant := parseEffectiveTierFromType(eq.Type)
	qualityIP := qualityIPMap[eq.Quality]
	masteryModifier := masteryModifierMap[tier]

	return baseIP + overchargeIP + (float64(tier+enchant)-4.0)*100 + qualityIP + specIP + (specIP * masteryModifier)
}

func parseEffectiveTierFromType(eqType string) (int, int) {
	enchant := 0
	tier, _ := strconv.Atoi(eqType[1:2])

	parts := strings.Split(eqType, "@")
	if len(parts) > 1 {
		enchant, _ = strconv.Atoi(parts[1][0:1])
	}

	return tier, enchant
}

func getBaseIPForItemType(eqType string) float64 {
	parts := strings.Split(eqType, "@")
	name := parts[0]
	nameParts := strings.Split(name, "_")
	artifactClass := nameParts[len(nameParts)-1]

	return 700.0 + artifactBaseBonusMap[artifactClass]
}

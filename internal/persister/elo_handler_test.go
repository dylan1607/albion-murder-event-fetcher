package persister

import (
	"albion-event-fetcher/internal/models"
	"encoding/json"
	"testing"
)

func TestParseEffectiveTierFromType_WithEnchant(t *testing.T) {
	// Arrange
	eqType := "T5_2H_KNUCKLES_SET2@1"

	// Act
	tier, enchant := parseEffectiveTierFromType(eqType)

	// Assert
	if tier != 5 {
		t.Fatalf("Expected tier to be 5, got %d\n", tier)
	}

	if enchant != 1 {
		t.Fatalf("Expected enchant to be 1, got %d\n", enchant)
	}
}

func TestParseEffectiveTierFromType_WithoutEnchant(t *testing.T) {
	// Arrange
	eqType := "T4_HEAD_CLOTH_SET3"

	// Act
	tier, enchant := parseEffectiveTierFromType(eqType)

	// Assert
	if tier != 4 {
		t.Fatalf("Expected tier to be 4, got %d\n", tier)
	}

	if enchant != 0 {
		t.Fatalf("Expected enchant to be 0, got %d\n", enchant)
	}
}

func TestParseEffectiveTierFromType_Malformed(t *testing.T) {
	// Arrange
	eqType := "ASDF_ASDF"

	// Act
	tier, enchant := parseEffectiveTierFromType(eqType)

	// Assert
	if tier != 0 {
		t.Fatalf("Expected tier to be 0, got %d\n", tier)
	}

	if enchant != 0 {
		t.Fatalf("Expected enchant to be 0, got %d\n", enchant)
	}
}

func TestApplySlayerSoftcap_Below1300(t *testing.T) {
	// Arrange
	openWorldIP := 750.0

	// Act
	cappedIP := applySlayerSoftcap(openWorldIP)

	// Assert
	if cappedIP != openWorldIP {
		t.Fatalf("Expected capped IP below 1300 to match initial IP, %f became %f\n", openWorldIP, cappedIP)
	}
}

func TestApplySlayerSoftcap_At1300(t *testing.T) {
	// Arrange
	openWorldIP := 1300.0

	// Act
	cappedIP := applySlayerSoftcap(openWorldIP)

	// Assert
	if cappedIP != openWorldIP {
		t.Fatalf("Expected capped IP to match initial at threshold, but initial IP of %f became %f\n", openWorldIP, cappedIP)
	}
}

func TestApplySlayerSoftcap_Above1300(t *testing.T) {
	// Arrange
	openWorldIP := 1600.0

	// Act
	cappedIP := applySlayerSoftcap(openWorldIP)

	// Assert
	if cappedIP != 1450 {
		t.Fatalf("Expected 1600 IP to be soft capped to 1450, got %f", cappedIP)
	}
}

func TestExpectedAverageEquipmentIP_Empty(t *testing.T) {
	// Arrange
	eq := models.Equipment{}

	// Act
	ip := expectedAverageEquipmentIP(eq, 0.0, false)

	// Assert
	if ip != 0 {
		t.Fatalf("Expected empty equipment to give 0 IP, got %f", ip)
	}
}

func TestExpectedAverageEquipmentIP_T4Quality(t *testing.T) {
	// Arrange
	eqs := []struct {
		eq models.Equipment
		ip float64
	}{
		// Normal
		{eq: models.Equipment{Type: "T4_ITEM", Quality: 0}, ip: 700.0},
		// Good
		{eq: models.Equipment{Type: "T4_ITEM", Quality: 1}, ip: 700.0 + 20.0},
		// Outstanding
		{eq: models.Equipment{Type: "T4_ITEM", Quality: 2}, ip: 700.0 + 40.0},
		// Excellent
		{eq: models.Equipment{Type: "T4_ITEM", Quality: 3}, ip: 700.0 + 60.0},
		// Masterpiece
		{eq: models.Equipment{Type: "T4_ITEM", Quality: 4}, ip: 700.0 + 100.0},
	}

	for _, eq := range eqs {
		// Act
		ip := expectedAverageEquipmentIP(eq.eq, 0.0, false)

		// Assert
		if ip != eq.ip {
			t.Fatalf("Expected %f, got %f", eq.ip, ip)
		}
	}

}

func TestExpectedAverageEquipmentIP_T4Enchant(t *testing.T) {
	// Arrange
	eqs := []struct {
		eq models.Equipment
		ip float64
	}{
		{eq: models.Equipment{Type: "T4_ITEM"}, ip: 700.0},
		{eq: models.Equipment{Type: "T4_ITEM@1"}, ip: 700.0 + 100.0},
		{eq: models.Equipment{Type: "T4_ITEM@2"}, ip: 700.0 + 200.0},
		{eq: models.Equipment{Type: "T4_ITEM@3"}, ip: 700.0 + 300.0},
	}

	for _, eq := range eqs {
		// Act
		ip := expectedAverageEquipmentIP(eq.eq, 0.0, false)

		// Assert
		if ip != eq.ip {
			t.Fatalf("Expected item %s to give %f, got %f", eq.eq.Type, eq.ip, ip)
		}
	}

}

func TestExpectedAverageEquipmentIP_Tiers(t *testing.T) {
	// Arrange
	eqs := []struct {
		eq models.Equipment
		ip float64
	}{
		{eq: models.Equipment{Type: "T4_ITEM"}, ip: 700.0},
		{eq: models.Equipment{Type: "T5_ITEM"}, ip: 800.0},
		{eq: models.Equipment{Type: "T6_ITEM"}, ip: 900.0},
		{eq: models.Equipment{Type: "T7_ITEM"}, ip: 1000.0},
		{eq: models.Equipment{Type: "T8_ITEM"}, ip: 1100.0},
	}

	for _, eq := range eqs {
		// Act
		ip := expectedAverageEquipmentIP(eq.eq, 0.0, false)

		// Assert
		if ip != eq.ip {
			t.Fatalf("For %s with quality %d, expected %f, got %f", eq.eq.Type, eq.eq.Quality, eq.ip, ip)
		}
	}
}

func TestExpectedAverageEquipmentIP_Max(t *testing.T) {
	// Arrange
	eq := models.Equipment{Type: "T8_ITEM@3", Quality: 4}

	// Act
	ip := expectedAverageEquipmentIP(eq, maxMasteryBonus, false)

	// Assert
	max := 1100 + 300 + 100 + maxMasteryBonus + (maxMasteryBonus * 0.2)
	if ip != max {
		t.Fatalf("Expected maximum possible IP to be %f, got %f", max, ip)
	}
}

func TestMaxAverageOpenWorldIP_HeadOnly(t *testing.T) {
	// Arrange
	eq := models.PlayerEquipment{
		Head: models.Equipment{Type: "T8_ITEM"},
	}

	// Act
	ip := maxAverageOpenWorldIP(eq)

	// Assert
	expected := (1100.0 + 100 + maxMasteryBonus + (maxMasteryBonus * 0.2)) / 6
	if ip != expected {
		t.Fatalf("Expected maximum IP to be %f, got %f", expected, ip)
	}
}

func TestMaxAverageOpenWorldIP_2H(t *testing.T) {
	// Arrange
	eq := models.PlayerEquipment{
		MainHand: models.Equipment{Type: "T8_2H_ITEM"},
	}

	// Act
	ip := maxAverageOpenWorldIP(eq)

	// Assert
	expectedWeaponIP := 1100.0 + 100 + maxMasteryBonus + (maxMasteryBonus * 0.2)
	expected := (2.0 * expectedWeaponIP) / 6.0
	if ip != expected {
		t.Fatalf("Expected maximum IP to be %f, got %f", expected, ip)
	}
}

func TestMaxAverageOpenWorldIP_1H_no_offhand(t *testing.T) {
	// Arrange
	eq := models.PlayerEquipment{
		MainHand: models.Equipment{Type: "T8_ITEM"},
	}

	// Act
	ip := maxAverageOpenWorldIP(eq)

	// Assert
	expectedWeaponIP := 1100.0 + 100 + maxMasteryBonus + (maxMasteryBonus * 0.2)
	expected := expectedWeaponIP / 6.0
	if ip != expected {
		t.Fatalf("Expected maximum IP to be %f, got %f", expected, ip)
	}
}

func TestMaxAverageOpenWorldIP_1H_with_offhand(t *testing.T) {
	// Arrange
	eq := models.PlayerEquipment{
		MainHand: models.Equipment{Type: "T8_ITEM"},
		OffHand:  models.Equipment{Type: "T4_ITEM"},
	}

	// Act
	ip := maxAverageOpenWorldIP(eq)

	// Assert
	expectedWeaponIP := 1100.0 + 100 + maxMasteryBonus + (maxMasteryBonus * 0.2)
	expectedOffhandIP := 700.0 + 100 + maxMasteryBonus
	expected := (expectedWeaponIP + expectedOffhandIP) / 6.0
	if ip != expected {
		t.Fatalf("Expected maximum IP to be %f, got %f", expected, ip)
	}
}

func TestIs1v1SlayerEvent_MisclassifiedSlayerEvents(t *testing.T) {
	for _, eventJSON := range nonSlayerEvents {
		var event models.Event
		err := json.Unmarshal([]byte(eventJSON), &event)
		if err != nil {
			t.Fatal("Failed to parse test event", err)
		}

		isSlayer := is1v1SlayerEvent(&event)

		if isSlayer {
			t.Fatalf("Non slayer CD event with ID %d was classified as slayer", event.EventID)
		}
	}
}

func TestIs1v1SlayerEvent_ConfirmedSlayerEvents(t *testing.T) {
	for _, eventJSON := range slayerEvents {
		var event models.Event
		err := json.Unmarshal([]byte(eventJSON), &event)
		if err != nil {
			t.Fatal("Failed to parse test event", err)
		}

		isSlayer := is1v1SlayerEvent(&event)

		if !isSlayer {
			t.Fatalf("Slayer CD event with ID %d was not classified as slayer", event.EventID)
		}
	}
}

var nonSlayerEvents = []string{
// 	`{
//   "groupMemberCount": 1,
//   "numberOfParticipants": 1,
//   "EventId": 547766156,
//   "TimeStamp": "2022-08-04T11:56:08.838675800Z",
//   "Version": 4,
//   "Killer": {
//     "AverageItemPower": 1464.23181,
//     "Equipment": {
//       "MainHand": {
//         "Type": "T7_MAIN_RAPIER_MORGANA@1",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "OffHand": {
//         "Type": "T5_OFF_DEMONSKULL_HELL@3",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Head": {
//         "Type": "T6_HEAD_CLOTH_SET3@3",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Armor": {
//         "Type": "T7_ARMOR_LEATHER_SET3@1",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Shoes": {
//         "Type": "T6_SHOES_PLATE_SET1@2",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Bag": {
//         "Type": "T6_BAG@1",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Cape": {
//         "Type": "T4_CAPEITEM_FW_THETFORD@3",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Mount": {
//         "Type": "T7_MOUNT_TERRORBIRD_ADC",
//         "Count": 1,
//         "Quality": 1,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Potion": {
//         "Type": "T8_POTION_COOLDOWN",
//         "Count": 5,
//         "Quality": 0,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Food": {
//         "Type": "T8_MEAL_STEW",
//         "Count": 4,
//         "Quality": 0,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       }
//     },
//     "Inventory": [],
//     "Name": "Antoh111",
//     "Id": "XoZKEo8hRgWIOsPL1PkDmg",
//     "GuildName": "Asgaroth",
//     "GuildId": "IvWd4XH8RpuOP4E0199Qaw",
//     "AllianceName": "1BBB1",
//     "AllianceId": "LS1iqKqdQbuR2illi-XyCg",
//     "AllianceTag": "",
//     "Avatar": "AVATAR_WOLF_01",
//     "AvatarRing": "AVATARRING_ADC_TOKENLOCKED_FIRE",
//     "DeathFame": 0,
//     "KillFame": 72504,
//     "FameRatio": 725039.99
//   },
//   "Victim": {
//     "AverageItemPower": 1352.83582,
//     "Equipment": {
//       "MainHand": {
//         "Type": "T7_2H_WARBOW",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "OffHand": null,
//       "Head": {
//         "Type": "T7_HEAD_CLOTH_SET3",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Armor": {
//         "Type": "T7_ARMOR_LEATHER_SET3",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Shoes": {
//         "Type": "T7_SHOES_LEATHER_SET3",
//         "Count": 1,
//         "Quality": 4,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Bag": null,
//       "Cape": {
//         "Type": "T4_CAPEITEM_UNDEAD",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Mount": {
//         "Type": "T5_MOUNT_HORSE",
//         "Count": 1,
//         "Quality": 1,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Potion": {
//         "Type": "T6_POTION_HEAL",
//         "Count": 1,
//         "Quality": 0,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Food": {
//         "Type": "T7_MEAL_OMELETTE",
//         "Count": 1,
//         "Quality": 0,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       }
//     },
//     "Inventory": [],
//     "Name": "Fastorius",
//     "Id": "mWU4fosmRzS-VZuR886pFA",
//     "GuildName": "HAMMER-FALL",
//     "GuildId": "bUFt0GwTTGqg2usm7v2uNg",
//     "AllianceName": "W00D",
//     "AllianceId": "2XhKRo-jTE2tRKIy8Xw6Pw",
//     "AllianceTag": "",
//     "Avatar": "AVATAR_ANNIVERSARY_2022",
//     "AvatarRing": "AVATARRING_ANNIVERSARY_2022",
//     "DeathFame": 72504,
//     "KillFame": 0,
//     "FameRatio": 0
//   },
//   "TotalVictimKillFame": 72504,
//   "Location": null,
//   "Participants": [
//     {
//       "AverageItemPower": 1464.23181,
//       "Equipment": {
//         "MainHand": {
//           "Type": "T7_MAIN_RAPIER_MORGANA@1",
//           "Count": 1,
//           "Quality": 4,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "OffHand": {
//           "Type": "T5_OFF_DEMONSKULL_HELL@3",
//           "Count": 1,
//           "Quality": 4,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Head": {
//           "Type": "T6_HEAD_CLOTH_SET3@3",
//           "Count": 1,
//           "Quality": 4,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Armor": {
//           "Type": "T7_ARMOR_LEATHER_SET3@1",
//           "Count": 1,
//           "Quality": 4,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Shoes": {
//           "Type": "T6_SHOES_PLATE_SET1@2",
//           "Count": 1,
//           "Quality": 4,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Bag": {
//           "Type": "T6_BAG@1",
//           "Count": 1,
//           "Quality": 4,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Cape": {
//           "Type": "T4_CAPEITEM_FW_THETFORD@3",
//           "Count": 1,
//           "Quality": 4,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Mount": {
//           "Type": "T7_MOUNT_TERRORBIRD_ADC",
//           "Count": 1,
//           "Quality": 1,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Potion": {
//           "Type": "T8_POTION_COOLDOWN",
//           "Count": 5,
//           "Quality": 0,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Food": {
//           "Type": "T8_MEAL_STEW",
//           "Count": 4,
//           "Quality": 0,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         }
//       },
//       "Inventory": [],
//       "Name": "Antoh111",
//       "Id": "XoZKEo8hRgWIOsPL1PkDmg",
//       "GuildName": "Asgaroth",
//       "GuildId": "IvWd4XH8RpuOP4E0199Qaw",
//       "AllianceName": "1BBB1",
//       "AllianceId": "LS1iqKqdQbuR2illi-XyCg",
//       "AllianceTag": "",
//       "Avatar": "AVATAR_WOLF_01",
//       "AvatarRing": "AVATARRING_ADC_TOKENLOCKED_FIRE",
//       "DeathFame": 0,
//       "KillFame": 0,
//       "FameRatio": 0,
//       "DamageDone": 6630,
//       "SupportHealingDone": 0
//     }
//   ],
//   "GroupMembers": [
//     {
//       "AverageItemPower": 0,
//       "Equipment": {
//         "MainHand": {
//           "Type": "T7_MAIN_RAPIER_MORGANA@1",
//           "Count": 1,
//           "Quality": 4,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "OffHand": null,
//         "Head": null,
//         "Armor": null,
//         "Shoes": null,
//         "Bag": null,
//         "Cape": null,
//         "Mount": null,
//         "Potion": null,
//         "Food": null
//       },
//       "Inventory": [],
//       "Name": "Antoh111",
//       "Id": "XoZKEo8hRgWIOsPL1PkDmg",
//       "GuildName": "Asgaroth",
//       "GuildId": "IvWd4XH8RpuOP4E0199Qaw",
//       "AllianceName": "1BBB1",
//       "AllianceId": "LS1iqKqdQbuR2illi-XyCg",
//       "AllianceTag": "",
//       "Avatar": "AVATAR_WOLF_01",
//       "AvatarRing": "AVATARRING_ADC_TOKENLOCKED_FIRE",
//       "DeathFame": 0,
//       "KillFame": 72504,
//       "FameRatio": 725039.99
//     }
//   ],
//   "GvGMatch": null,
//   "BattleId": 547766156,
//   "KillArea": "OPEN_WORLD",
//   "Category": null,
//   "Type": "KILL"
// }`,
	`{
  "groupMemberCount": 1,
  "numberOfParticipants": 1,
  "EventId": 585203403,
  "TimeStamp": "2022-09-12T22:38:09.185880600Z",
  "Version": 4,
  "Killer": {
    "AverageItemPower": 1573.22327,
    "Equipment": {
      "MainHand": {
        "Type": "T7_MAIN_RAPIER_MORGANA@2",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "OffHand": {
        "Type": "T6_OFF_TOTEM_KEEPER@3",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Head": {
        "Type": "T6_HEAD_CLOTH_SET3@3",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Armor": {
        "Type": "T6_ARMOR_LEATHER_SET3@3",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Shoes": {
        "Type": "T6_SHOES_LEATHER_AVALON@3",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Bag": {
        "Type": "T6_BAG@1",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Cape": {
        "Type": "T5_CAPEITEM_UNDEAD@3",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Mount": {
        "Type": "T6_MOUNT_GIANTSTAG_MOOSE",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Potion": {
        "Type": "T8_POTION_CLEANSE",
        "Count": 10,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Food": {
        "Type": "T5_MEAL_SOUP",
        "Count": 5,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      }
    },
    "Inventory": [],
    "Name": "MIshania228",
    "Id": "zYDZ-eNeQ_2ItjvQl7VrNg",
    "GuildName": "Dead Spirit",
    "GuildId": "zB7OpFcESvWTVOkkv1MjkA",
    "AllianceName": "WARS",
    "AllianceId": "Pklc9G8eRL-jROPv6VtCPA",
    "AllianceTag": "",
    "Avatar": "AVATAR_HARVESTER_01",
    "AvatarRing": "AVATARRING_ANNIVERSARY_2022",
    "DeathFame": 0,
    "KillFame": 468341,
    "FameRatio": 4683409.93,
    "LifetimeStatistics": {
      "PvE": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0,
        "Hellgate": 0,
        "CorruptedDungeon": 0
      },
      "Gathering": {
        "Fiber": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Hide": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Ore": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Rock": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Wood": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "All": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        }
      },
      "Crafting": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0
      },
      "CrystalLeague": 0,
      "FishingFame": 0,
      "FarmingFame": 0,
      "Timestamp": null
    }
  },
  "Victim": {
    "AverageItemPower": 1469.86511,
    "Equipment": {
      "MainHand": {
        "Type": "T6_MAIN_RAPIER_MORGANA@2",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "OffHand": {
        "Type": "T5_OFF_TOTEM_KEEPER@3",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Head": {
        "Type": "T8_HEAD_CLOTH_SET2",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Armor": {
        "Type": "T8_ARMOR_LEATHER_SET3",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Shoes": {
        "Type": "T6_SHOES_LEATHER_AVALON@2",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Bag": {
        "Type": "T5_BAG@1",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Cape": {
        "Type": "T4_CAPEITEM_UNDEAD@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Mount": {
        "Type": "T6_MOUNT_GIANTSTAG_MOOSE",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Potion": {
        "Type": "T8_POTION_CLEANSE",
        "Count": 7,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Food": {
        "Type": "T5_MEAL_SOUP",
        "Count": 3,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      }
    },
    "Inventory": [
      {
        "Type": "T6_RUNE",
        "Count": 41,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_TRIBAL_RARITY2",
        "Count": 4,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_RELIC",
        "Count": 24,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_RUNE",
        "Count": 251,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_RUNE",
        "Count": 89,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T1_SILVERBAG_NONTRADABLE",
        "Count": 249,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_RELIC",
        "Count": 18,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_SOUL",
        "Count": 9,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_SOUL",
        "Count": 84,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_SOUL",
        "Count": 34,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_RELIC",
        "Count": 4,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T7_SOUL",
        "Count": 2,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "QUESTITEM_EXP_TOKEN_D5_T6_EXP_HRD_HERETIC_LUMBERCAMP",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_TRIBAL_RARITY1",
        "Count": 2,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_SILVERWARE_RARITY2",
        "Count": 2,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_OFF_BOOK",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_2H_ARCANESTAFF",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_HEAD_PLATE_SET3",
        "Count": 2,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_SILVERBAG_NONTRADABLE",
        "Count": 55,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_MAIN_SWORD@2",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_2H_HOLYSTAFF",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T8_RUNE",
        "Count": 3,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_CAPE@1",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T7_RUNE",
        "Count": 5,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_KNOWLEDGE_RARITY1",
        "Count": 8,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_CEREMONIAL_RARITY1",
        "Count": 10,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_RITUAL_RARITY1",
        "Count": 6,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_DECORATIVE_RARITY1",
        "Count": 3,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_CEREMONIAL_RARITY2",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_HEAD_PLATE_SET3",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_ARMOR_PLATE_SET1",
        "Count": 2,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_ARMOR_LEATHER_SET1",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_TRIBAL_RARITY3",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_SILVERWARE_RARITY1",
        "Count": 4,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_RITUAL_RARITY2",
        "Count": 2,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_ARMOR_LEATHER_SET3@2",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_ARMOR_LEATHER_SET2",
        "Count": 2,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_2H_DOUBLEBLADEDSTAFF@1",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_HEAD_CLOTH_KEEPER",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_ARMOR_CLOTH_SET1",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_2H_DUALSWORD",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_OFF_HORN_KEEPER",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_2H_CLAYMORE@1",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_HEAD_PLATE_SET2",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_DECORATIVE_RARITY2",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_2H_GLAIVE@1",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_SKILLBOOK_STANDARD",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      null
    ],
    "Name": "EriCWaldo",
    "Id": "1ZW7ST4iSOG3EU3ObyACxw",
    "GuildName": "HOT and RUN",
    "GuildId": "lJtn-zDORxeXvDEI2fUvjA",
    "AllianceName": "",
    "AllianceId": "",
    "AllianceTag": "",
    "Avatar": "AVATAR_ADC_TOKENLOCKED_01",
    "AvatarRing": "AVATARRING_ADC_TOKENLOCKED_ARCANE",
    "DeathFame": 468341,
    "KillFame": 0,
    "FameRatio": 0,
    "LifetimeStatistics": {
      "PvE": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0,
        "Hellgate": 0,
        "CorruptedDungeon": 0
      },
      "Gathering": {
        "Fiber": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Hide": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Ore": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Rock": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Wood": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "All": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        }
      },
      "Crafting": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0
      },
      "CrystalLeague": 0,
      "FishingFame": 0,
      "FarmingFame": 0,
      "Timestamp": null
    }
  },
  "TotalVictimKillFame": 468341,
  "Location": null,
  "Participants": [
    {
      "AverageItemPower": 1573.22327,
      "Equipment": {
        "MainHand": {
          "Type": "T7_MAIN_RAPIER_MORGANA@2",
          "Count": 1,
          "Quality": 3,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "OffHand": {
          "Type": "T6_OFF_TOTEM_KEEPER@3",
          "Count": 1,
          "Quality": 2,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Head": {
          "Type": "T6_HEAD_CLOTH_SET3@3",
          "Count": 1,
          "Quality": 2,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Armor": {
          "Type": "T6_ARMOR_LEATHER_SET3@3",
          "Count": 1,
          "Quality": 2,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Shoes": {
          "Type": "T6_SHOES_LEATHER_AVALON@3",
          "Count": 1,
          "Quality": 2,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Bag": {
          "Type": "T6_BAG@1",
          "Count": 1,
          "Quality": 1,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Cape": {
          "Type": "T5_CAPEITEM_UNDEAD@3",
          "Count": 1,
          "Quality": 2,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Mount": {
          "Type": "T6_MOUNT_GIANTSTAG_MOOSE",
          "Count": 1,
          "Quality": 1,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Potion": {
          "Type": "T8_POTION_CLEANSE",
          "Count": 10,
          "Quality": 0,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Food": {
          "Type": "T5_MEAL_SOUP",
          "Count": 5,
          "Quality": 0,
          "ActiveSpells": [],
          "PassiveSpells": []
        }
      },
      "Inventory": [],
      "Name": "MIshania228",
      "Id": "zYDZ-eNeQ_2ItjvQl7VrNg",
      "GuildName": "Dead Spirit",
      "GuildId": "zB7OpFcESvWTVOkkv1MjkA",
      "AllianceName": "WARS",
      "AllianceId": "Pklc9G8eRL-jROPv6VtCPA",
      "AllianceTag": "",
      "Avatar": "AVATAR_HARVESTER_01",
      "AvatarRing": "AVATARRING_ANNIVERSARY_2022",
      "DeathFame": 0,
      "KillFame": 0,
      "FameRatio": 0,
      "LifetimeStatistics": {
        "PvE": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0,
          "Hellgate": 0,
          "CorruptedDungeon": 0
        },
        "Gathering": {
          "Fiber": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Hide": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Ore": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Rock": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Wood": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "All": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          }
        },
        "Crafting": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "CrystalLeague": 0,
        "FishingFame": 0,
        "FarmingFame": 0,
        "Timestamp": null
      },
      "DamageDone": 3227,
      "SupportHealingDone": 0
    }
  ],
  "GroupMembers": [
    {
      "AverageItemPower": 0,
      "Equipment": {
        "MainHand": {
          "Type": "T7_MAIN_RAPIER_MORGANA@2",
          "Count": 1,
          "Quality": 3,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "OffHand": null,
        "Head": null,
        "Armor": null,
        "Shoes": null,
        "Bag": null,
        "Cape": null,
        "Mount": null,
        "Potion": null,
        "Food": null
      },
      "Inventory": [],
      "Name": "MIshania228",
      "Id": "zYDZ-eNeQ_2ItjvQl7VrNg",
      "GuildName": "Dead Spirit",
      "GuildId": "zB7OpFcESvWTVOkkv1MjkA",
      "AllianceName": "WARS",
      "AllianceId": "Pklc9G8eRL-jROPv6VtCPA",
      "AllianceTag": "",
      "Avatar": "AVATAR_HARVESTER_01",
      "AvatarRing": "AVATARRING_ANNIVERSARY_2022",
      "DeathFame": 0,
      "KillFame": 468341,
      "FameRatio": 4683409.93,
      "LifetimeStatistics": {
        "PvE": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0,
          "Hellgate": 0,
          "CorruptedDungeon": 0
        },
        "Gathering": {
          "Fiber": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Hide": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Ore": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Rock": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Wood": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "All": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          }
        },
        "Crafting": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "CrystalLeague": 0,
        "FishingFame": 0,
        "FarmingFame": 0,
        "Timestamp": null
      }
    }
  ],
  "GvGMatch": null,
  "BattleId": 585203243,
  "KillArea": "OPEN_WORLD",
  "Category": null,
  "Type": "KILL"
}`,
// 	`{
//   "groupMemberCount": 1,
//   "numberOfParticipants": 1,
//   "EventId": 563091678,
//   "TimeStamp": "2022-08-20T04:17:39.693252300Z",
//   "Version": 4,
//   "Killer": {
//     "AverageItemPower": 1453.65332,
//     "Equipment": {
//       "MainHand": {
//         "Type": "T8_2H_DEMONICSTAFF",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "OffHand": null,
//       "Head": {
//         "Type": "T8_HEAD_CLOTH_SET2",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Armor": {
//         "Type": "T8_ARMOR_CLOTH_SET2",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Shoes": {
//         "Type": "T8_SHOES_PLATE_SET1",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Bag": {
//         "Type": "T8_BAG",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Cape": {
//         "Type": "T8_CAPE@3",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Mount": {
//         "Type": "T8_MOUNT_ARMORED_HORSE",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Potion": null,
//       "Food": null
//     },
//     "Inventory": [],
//     "Name": "Glow",
//     "Id": "cu49ypI6RBq3XLSiaSF--A",
//     "GuildName": "",
//     "GuildId": "",
//     "AllianceName": "",
//     "AllianceId": "",
//     "AllianceTag": "",
//     "Avatar": "AVATAR_06",
//     "AvatarRing": "AVATARRING_ADC_MAY2019",
//     "DeathFame": 0,
//     "KillFame": 17738526,
//     "FameRatio": 177385257.36,
//     "LifetimeStatistics": {
//       "PvE": {
//         "Total": 0,
//         "Royal": 0,
//         "Outlands": 0,
//         "Avalon": 0,
//         "Hellgate": 0,
//         "CorruptedDungeon": 0
//       },
//       "Gathering": {
//         "Fiber": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "Hide": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "Ore": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "Rock": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "Wood": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "All": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         }
//       },
//       "Crafting": {
//         "Total": 0,
//         "Royal": 0,
//         "Outlands": 0,
//         "Avalon": 0
//       },
//       "CrystalLeague": 0,
//       "FishingFame": 0,
//       "FarmingFame": 0,
//       "Timestamp": null
//     }
//   },
//   "Victim": {
//     "AverageItemPower": 1582.23987,
//     "Equipment": {
//       "MainHand": {
//         "Type": "T8_MAIN_SWORD@3",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "OffHand": {
//         "Type": "T8_OFF_SHIELD@3",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Head": {
//         "Type": "T8_HEAD_PLATE_SET1@3",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Armor": {
//         "Type": "T8_ARMOR_PLATE_SET1@3",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Shoes": {
//         "Type": "T8_SHOES_PLATE_SET1@3",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Bag": {
//         "Type": "T8_BAG@3",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Cape": {
//         "Type": "T8_CAPE",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Mount": {
//         "Type": "T3_MOUNT_HORSE",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       "Potion": null,
//       "Food": null
//     },
//     "Inventory": [
//       {
//         "Type": "T8_MAIN_RAPIER_MORGANA@3",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       {
//         "Type": "T8_MAIN_1HCROSSBOW@3",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       {
//         "Type": "T8_2H_DEMONICSTAFF@3",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       {
//         "Type": "T8_OFF_SHIELD@3",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       {
//         "Type": "T8_HEAD_CLOTH_SET2@3",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       {
//         "Type": "T8_ARMOR_CLOTH_SET2@3",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       {
//         "Type": "T8_ARMOR_LEATHER_SET3@3",
//         "Count": 1,
//         "Quality": 2,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       {
//         "Type": "T8_SHOES_LEATHER_ROYAL@3",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       {
//         "Type": "T8_SHOES_PLATE_SET1@3",
//         "Count": 1,
//         "Quality": 3,
//         "ActiveSpells": [],
//         "PassiveSpells": []
//       },
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null,
//       null
//     ],
//     "Name": "Seasonql",
//     "Id": "0fi-jEbvSli8ivRQreeA8Q",
//     "GuildName": "",
//     "GuildId": "",
//     "AllianceName": "",
//     "AllianceId": "",
//     "AllianceTag": "",
//     "Avatar": "AVATAR_01",
//     "AvatarRing": "RING1",
//     "DeathFame": 17738526,
//     "KillFame": 0,
//     "FameRatio": 0,
//     "LifetimeStatistics": {
//       "PvE": {
//         "Total": 0,
//         "Royal": 0,
//         "Outlands": 0,
//         "Avalon": 0,
//         "Hellgate": 0,
//         "CorruptedDungeon": 0
//       },
//       "Gathering": {
//         "Fiber": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "Hide": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "Ore": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "Rock": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "Wood": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "All": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         }
//       },
//       "Crafting": {
//         "Total": 0,
//         "Royal": 0,
//         "Outlands": 0,
//         "Avalon": 0
//       },
//       "CrystalLeague": 0,
//       "FishingFame": 0,
//       "FarmingFame": 0,
//       "Timestamp": null
//     }
//   },
//   "TotalVictimKillFame": 17738526,
//   "Location": null,
//   "Participants": [
//     {
//       "AverageItemPower": 1453.65332,
//       "Equipment": {
//         "MainHand": {
//           "Type": "T8_2H_DEMONICSTAFF",
//           "Count": 1,
//           "Quality": 3,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "OffHand": null,
//         "Head": {
//           "Type": "T8_HEAD_CLOTH_SET2",
//           "Count": 1,
//           "Quality": 3,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Armor": {
//           "Type": "T8_ARMOR_CLOTH_SET2",
//           "Count": 1,
//           "Quality": 3,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Shoes": {
//           "Type": "T8_SHOES_PLATE_SET1",
//           "Count": 1,
//           "Quality": 2,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Bag": {
//           "Type": "T8_BAG",
//           "Count": 1,
//           "Quality": 2,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Cape": {
//           "Type": "T8_CAPE@3",
//           "Count": 1,
//           "Quality": 2,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Mount": {
//           "Type": "T8_MOUNT_ARMORED_HORSE",
//           "Count": 1,
//           "Quality": 2,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "Potion": null,
//         "Food": null
//       },
//       "Inventory": [],
//       "Name": "Glow",
//       "Id": "cu49ypI6RBq3XLSiaSF--A",
//       "GuildName": "",
//       "GuildId": "",
//       "AllianceName": "",
//       "AllianceId": "",
//       "AllianceTag": "",
//       "Avatar": "AVATAR_06",
//       "AvatarRing": "AVATARRING_ADC_MAY2019",
//       "DeathFame": 0,
//       "KillFame": 0,
//       "FameRatio": 0,
//       "LifetimeStatistics": {
//         "PvE": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0,
//           "Hellgate": 0,
//           "CorruptedDungeon": 0
//         },
//         "Gathering": {
//           "Fiber": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "Hide": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "Ore": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "Rock": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "Wood": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "All": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           }
//         },
//         "Crafting": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "CrystalLeague": 0,
//         "FishingFame": 0,
//         "FarmingFame": 0,
//         "Timestamp": null
//       },
//       "DamageDone": 3296,
//       "SupportHealingDone": 0
//     }
//   ],
//   "GroupMembers": [
//     {
//       "AverageItemPower": 0,
//       "Equipment": {
//         "MainHand": {
//           "Type": "T8_2H_DEMONICSTAFF",
//           "Count": 1,
//           "Quality": 3,
//           "ActiveSpells": [],
//           "PassiveSpells": []
//         },
//         "OffHand": null,
//         "Head": null,
//         "Armor": null,
//         "Shoes": null,
//         "Bag": null,
//         "Cape": null,
//         "Mount": null,
//         "Potion": null,
//         "Food": null
//       },
//       "Inventory": [],
//       "Name": "Glow",
//       "Id": "cu49ypI6RBq3XLSiaSF--A",
//       "GuildName": "",
//       "GuildId": "",
//       "AllianceName": "",
//       "AllianceId": "",
//       "AllianceTag": "",
//       "Avatar": "AVATAR_06",
//       "AvatarRing": "AVATARRING_ADC_MAY2019",
//       "DeathFame": 0,
//       "KillFame": 17738526,
//       "FameRatio": 177385257.36,
//       "LifetimeStatistics": {
//         "PvE": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0,
//           "Hellgate": 0,
//           "CorruptedDungeon": 0
//         },
//         "Gathering": {
//           "Fiber": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "Hide": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "Ore": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "Rock": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "Wood": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           },
//           "All": {
//             "Total": 0,
//             "Royal": 0,
//             "Outlands": 0,
//             "Avalon": 0
//           }
//         },
//         "Crafting": {
//           "Total": 0,
//           "Royal": 0,
//           "Outlands": 0,
//           "Avalon": 0
//         },
//         "CrystalLeague": 0,
//         "FishingFame": 0,
//         "FarmingFame": 0,
//         "Timestamp": null
//       }
//     }
//   ],
//   "GvGMatch": null,
//   "BattleId": 563091678,
//   "KillArea": "OPEN_WORLD",
//   "Category": null,
//   "Type": "KILL"
// }`,
}

var slayerEvents = []string{
	`{
  "groupMemberCount": 1,
  "numberOfParticipants": 1,
  "EventId": 585279035,
  "TimeStamp": "2022-09-13T00:55:23.856175800Z",
  "Version": 4,
  "Killer": {
    "AverageItemPower": 1426.21094,
    "Equipment": {
      "MainHand": {
        "Type": "T8_2H_DEMONICSTAFF@1",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "OffHand": null,
      "Head": {
        "Type": "T6_HEAD_CLOTH_HELL@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Armor": {
        "Type": "T8_ARMOR_PLATE_SET1@1",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Shoes": {
        "Type": "T8_SHOES_PLATE_SET2@1",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Bag": {
        "Type": "T5_BAG",
        "Count": 1,
        "Quality": 5,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Cape": {
        "Type": "T4_CAPEITEM_FW_MARTLOCK@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Mount": {
        "Type": "T4_MOUNT_GIANTSTAG",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Potion": {
        "Type": "T6_POTION_HEAL@1",
        "Count": 7,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Food": {
        "Type": "T7_MEAL_OMELETTE@2",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      }
    },
    "Inventory": [],
    "Name": "Danilskinem",
    "Id": "nRxLO4K1Sear_i9Y6VuzvA",
    "GuildName": "AII In",
    "GuildId": "SQLv8BpRT3uCKQyn8iFnZg",
    "AllianceName": "MANCO",
    "AllianceId": "j6_juPoYQniB13XenDrv3A",
    "AllianceTag": "",
    "Avatar": "AVATAR_HARVESTER_01",
    "AvatarRing": "AVATARRING_CONTENT_CREATOR",
    "DeathFame": 0,
    "KillFame": 178435,
    "FameRatio": 1784349.97,
    "LifetimeStatistics": {
      "PvE": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0,
        "Hellgate": 0,
        "CorruptedDungeon": 0
      },
      "Gathering": {
        "Fiber": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Hide": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Ore": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Rock": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Wood": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "All": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        }
      },
      "Crafting": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0
      },
      "CrystalLeague": 0,
      "FishingFame": 0,
      "FarmingFame": 0,
      "Timestamp": null
    }
  },
  "Victim": {
    "AverageItemPower": 1352.79419,
    "Equipment": {
      "MainHand": {
        "Type": "T7_2H_CLAYMORE",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "OffHand": null,
      "Head": {
        "Type": "T4_HEAD_CLOTH_MORGANA@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Armor": {
        "Type": "T6_ARMOR_LEATHER_SET1@1",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Shoes": {
        "Type": "T7_SHOES_PLATE_SET3",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Bag": {
        "Type": "T5_BAG",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Cape": {
        "Type": "T4_CAPEITEM_FW_THETFORD@3",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Mount": {
        "Type": "T4_MOUNT_HORSE",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Potion": null,
      "Food": null
    },
    "Inventory": [
      {
        "Type": "T6_POTION_HEAL",
        "Count": 3,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T7_POTION_STONESKIN",
        "Count": 3,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_POTION_COOLDOWN",
        "Count": 5,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T1_SILVERBAG_NONTRADABLE",
        "Count": 72,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_RUNE",
        "Count": 187,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_RUNE",
        "Count": 90,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_SKILLBOOK_STANDARD",
        "Count": 2,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T1_SKILLBOOK_NONTRADABLE",
        "Count": 56,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_RUNE",
        "Count": 12,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_SOUL",
        "Count": 29,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_HEAD_CLOTH_HELL@2",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_OFF_TOTEM_KEEPER@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_MAIN_CURSEDSTAFF@1",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_ARMOR_PLATE_SET1@2",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T7_SHOES_PLATE_SET2",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_BAG@1",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_MOUNT_HORSE",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_CAPEITEM_FW_MARTLOCK@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_HEAD_LEATHER_SET3@1",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_RELIC",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_SOUL",
        "Count": 2,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_SILVERWARE_RARITY1",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_OFF_TORCH",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_RELIC",
        "Count": 4,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_RELIC",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_CEREMONIAL_RARITY2",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_KNOWLEDGE_RARITY2",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T7_RELIC",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_ARMOR_CLOTH_SET3",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_ARMOR_CLOTH_SET2",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_SILVERBAG_NONTRADABLE",
        "Count": 7,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "TREASURE_TRIBAL_RARITY2",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_HEAD_CLOTH_SET2",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      null,
      null,
      null,
      null,
      null,
      {
        "Type": "T5_SOUL",
        "Count": 26,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ],
    "Name": "BUDIOS",
    "Id": "mbBKqadlRKGpB0F7mfC3vw",
    "GuildName": "I Pandora I",
    "GuildId": "1ACRIdr8TCuxz8x5_UdBIA",
    "AllianceName": "B0X",
    "AllianceId": "smDCiFppSv2dpde0BRShuQ",
    "AllianceTag": "",
    "Avatar": "GVGSEASON_16_GOLD",
    "AvatarRing": "AVATARRING_ADC_NOV2018",
    "DeathFame": 178435,
    "KillFame": 0,
    "FameRatio": 0,
    "LifetimeStatistics": {
      "PvE": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0,
        "Hellgate": 0,
        "CorruptedDungeon": 0
      },
      "Gathering": {
        "Fiber": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Hide": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Ore": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Rock": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Wood": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "All": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        }
      },
      "Crafting": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0
      },
      "CrystalLeague": 0,
      "FishingFame": 0,
      "FarmingFame": 0,
      "Timestamp": null
    }
  },
  "TotalVictimKillFame": 178435,
  "Location": null,
  "Participants": [
    {
      "AverageItemPower": 1426.21094,
      "Equipment": {
        "MainHand": {
          "Type": "T8_2H_DEMONICSTAFF@1",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "OffHand": null,
        "Head": {
          "Type": "T6_HEAD_CLOTH_HELL@3",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Armor": {
          "Type": "T8_ARMOR_PLATE_SET1@1",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Shoes": {
          "Type": "T8_SHOES_PLATE_SET2@1",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Bag": {
          "Type": "T5_BAG",
          "Count": 1,
          "Quality": 5,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Cape": {
          "Type": "T4_CAPEITEM_FW_MARTLOCK@3",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Mount": {
          "Type": "T4_MOUNT_GIANTSTAG",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Potion": {
          "Type": "T6_POTION_HEAL@1",
          "Count": 7,
          "Quality": 0,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Food": {
          "Type": "T7_MEAL_OMELETTE@2",
          "Count": 1,
          "Quality": 0,
          "ActiveSpells": [],
          "PassiveSpells": []
        }
      },
      "Inventory": [],
      "Name": "Danilskinem",
      "Id": "nRxLO4K1Sear_i9Y6VuzvA",
      "GuildName": "AII In",
      "GuildId": "SQLv8BpRT3uCKQyn8iFnZg",
      "AllianceName": "MANCO",
      "AllianceId": "j6_juPoYQniB13XenDrv3A",
      "AllianceTag": "",
      "Avatar": "AVATAR_HARVESTER_01",
      "AvatarRing": "AVATARRING_CONTENT_CREATOR",
      "DeathFame": 0,
      "KillFame": 0,
      "FameRatio": 0,
      "LifetimeStatistics": {
        "PvE": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0,
          "Hellgate": 0,
          "CorruptedDungeon": 0
        },
        "Gathering": {
          "Fiber": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Hide": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Ore": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Rock": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Wood": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "All": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          }
        },
        "Crafting": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "CrystalLeague": 0,
        "FishingFame": 0,
        "FarmingFame": 0,
        "Timestamp": null
      },
      "DamageDone": 4016,
      "SupportHealingDone": 0
    }
  ],
  "GroupMembers": [
    {
      "AverageItemPower": 0,
      "Equipment": {
        "MainHand": {
          "Type": "T8_2H_DEMONICSTAFF@1",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "OffHand": null,
        "Head": null,
        "Armor": null,
        "Shoes": null,
        "Bag": null,
        "Cape": null,
        "Mount": null,
        "Potion": null,
        "Food": null
      },
      "Inventory": [],
      "Name": "Danilskinem",
      "Id": "nRxLO4K1Sear_i9Y6VuzvA",
      "GuildName": "AII In",
      "GuildId": "SQLv8BpRT3uCKQyn8iFnZg",
      "AllianceName": "MANCO",
      "AllianceId": "j6_juPoYQniB13XenDrv3A",
      "AllianceTag": "",
      "Avatar": "AVATAR_HARVESTER_01",
      "AvatarRing": "AVATARRING_CONTENT_CREATOR",
      "DeathFame": 0,
      "KillFame": 178435,
      "FameRatio": 1784349.97,
      "LifetimeStatistics": {
        "PvE": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0,
          "Hellgate": 0,
          "CorruptedDungeon": 0
        },
        "Gathering": {
          "Fiber": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Hide": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Ore": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Rock": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Wood": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "All": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          }
        },
        "Crafting": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "CrystalLeague": 0,
        "FishingFame": 0,
        "FarmingFame": 0,
        "Timestamp": null
      }
    }
  ],
  "GvGMatch": null,
  "BattleId": 585279035,
  "KillArea": "OPEN_WORLD",
  "Category": null,
  "Type": "KILL"
}`,
	`{
  "groupMemberCount": 1,
  "numberOfParticipants": 1,
  "EventId": 585250100,
  "TimeStamp": "2022-09-13T00:02:14.355142200Z",
  "Version": 4,
  "Killer": {
    "AverageItemPower": 1444.13293,
    "Equipment": {
      "MainHand": {
        "Type": "T8_MAIN_SWORD",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "OffHand": {
        "Type": "T6_OFF_DEMONSKULL_HELL@2",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Head": {
        "Type": "T5_HEAD_CLOTH_MORGANA@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Armor": {
        "Type": "T8_ARMOR_LEATHER_SET1",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Shoes": {
        "Type": "T6_SHOES_PLATE_AVALON@2",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Bag": {
        "Type": "T5_BAG@1",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Cape": {
        "Type": "T5_CAPEITEM_FW_THETFORD@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Mount": {
        "Type": "T4_MOUNT_HORSE",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Potion": {
        "Type": "T6_POTION_HEAL@1",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Food": {
        "Type": "T5_MEAL_SOUP",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      }
    },
    "Inventory": [],
    "Name": "IOWF",
    "Id": "aU2WQHKKRe6sCn6l9oKg-A",
    "GuildName": "The Notorious Army",
    "GuildId": "iD0l4t-GT_CeMIRrabDljg",
    "AllianceName": "",
    "AllianceId": "",
    "AllianceTag": "",
    "Avatar": "GVGSEASON_09",
    "AvatarRing": "AVATARRING_CONTENT_CREATOR",
    "DeathFame": 0,
    "KillFame": 169209,
    "FameRatio": 1692089.97,
    "LifetimeStatistics": {
      "PvE": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0,
        "Hellgate": 0,
        "CorruptedDungeon": 0
      },
      "Gathering": {
        "Fiber": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Hide": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Ore": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Rock": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Wood": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "All": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        }
      },
      "Crafting": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0
      },
      "CrystalLeague": 0,
      "FishingFame": 0,
      "FarmingFame": 0,
      "Timestamp": null
    }
  },
  "Victim": {
    "AverageItemPower": 1363.14124,
    "Equipment": {
      "MainHand": {
        "Type": "T5_2H_BOW_KEEPER@2",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "OffHand": null,
      "Head": {
        "Type": "T8_HEAD_CLOTH_SET3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Armor": {
        "Type": "T5_ARMOR_PLATE_AVALON@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Shoes": {
        "Type": "T8_SHOES_PLATE_SET1",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Bag": {
        "Type": "T4_BAG",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Cape": {
        "Type": "T4_CAPEITEM_FW_THETFORD@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Mount": {
        "Type": "T4_MOUNT_GIANTSTAG",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Potion": {
        "Type": "T6_POTION_HEAL",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Food": null
    },
    "Inventory": [
      null,
      {
        "Type": "T7_MEAL_OMELETTE",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_MEAL_SOUP",
        "Count": 4,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_POTION_COOLDOWN",
        "Count": 7,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_POTION_COOLDOWN",
        "Count": 8,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T7_POTION_STONESKIN",
        "Count": 4,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ],
    "Name": "T1zXable",
    "Id": "WpYSyC9aRZ2syOlrU-OVEQ",
    "GuildName": "Old Time Classic",
    "GuildId": "Ikio1rADSe6bVTPug580mA",
    "AllianceName": "",
    "AllianceId": "",
    "AllianceTag": "",
    "Avatar": "AVATAR_07",
    "AvatarRing": "AVATARRING_ADC_JAN2019",
    "DeathFame": 169209,
    "KillFame": 0,
    "FameRatio": 0,
    "LifetimeStatistics": {
      "PvE": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0,
        "Hellgate": 0,
        "CorruptedDungeon": 0
      },
      "Gathering": {
        "Fiber": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Hide": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Ore": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Rock": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Wood": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "All": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        }
      },
      "Crafting": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0
      },
      "CrystalLeague": 0,
      "FishingFame": 0,
      "FarmingFame": 0,
      "Timestamp": null
    }
  },
  "TotalVictimKillFame": 169209,
  "Location": null,
  "Participants": [
    {
      "AverageItemPower": 1444.13293,
      "Equipment": {
        "MainHand": {
          "Type": "T8_MAIN_SWORD",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "OffHand": {
          "Type": "T6_OFF_DEMONSKULL_HELL@2",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Head": {
          "Type": "T5_HEAD_CLOTH_MORGANA@3",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Armor": {
          "Type": "T8_ARMOR_LEATHER_SET1",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Shoes": {
          "Type": "T6_SHOES_PLATE_AVALON@2",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Bag": {
          "Type": "T5_BAG@1",
          "Count": 1,
          "Quality": 2,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Cape": {
          "Type": "T5_CAPEITEM_FW_THETFORD@3",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Mount": {
          "Type": "T4_MOUNT_HORSE",
          "Count": 1,
          "Quality": 1,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Potion": {
          "Type": "T6_POTION_HEAL@1",
          "Count": 1,
          "Quality": 0,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Food": {
          "Type": "T5_MEAL_SOUP",
          "Count": 1,
          "Quality": 0,
          "ActiveSpells": [],
          "PassiveSpells": []
        }
      },
      "Inventory": [],
      "Name": "IOWF",
      "Id": "aU2WQHKKRe6sCn6l9oKg-A",
      "GuildName": "The Notorious Army",
      "GuildId": "iD0l4t-GT_CeMIRrabDljg",
      "AllianceName": "",
      "AllianceId": "",
      "AllianceTag": "",
      "Avatar": "GVGSEASON_09",
      "AvatarRing": "AVATARRING_CONTENT_CREATOR",
      "DeathFame": 0,
      "KillFame": 0,
      "FameRatio": 0,
      "LifetimeStatistics": {
        "PvE": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0,
          "Hellgate": 0,
          "CorruptedDungeon": 0
        },
        "Gathering": {
          "Fiber": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Hide": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Ore": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Rock": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Wood": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "All": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          }
        },
        "Crafting": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "CrystalLeague": 0,
        "FishingFame": 0,
        "FarmingFame": 0,
        "Timestamp": null
      },
      "DamageDone": 7744,
      "SupportHealingDone": 0
    }
  ],
  "GroupMembers": [
    {
      "AverageItemPower": 0,
      "Equipment": {
        "MainHand": {
          "Type": "T8_MAIN_SWORD",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "OffHand": null,
        "Head": null,
        "Armor": null,
        "Shoes": null,
        "Bag": null,
        "Cape": null,
        "Mount": null,
        "Potion": null,
        "Food": null
      },
      "Inventory": [],
      "Name": "IOWF",
      "Id": "aU2WQHKKRe6sCn6l9oKg-A",
      "GuildName": "The Notorious Army",
      "GuildId": "iD0l4t-GT_CeMIRrabDljg",
      "AllianceName": "",
      "AllianceId": "",
      "AllianceTag": "",
      "Avatar": "GVGSEASON_09",
      "AvatarRing": "AVATARRING_CONTENT_CREATOR",
      "DeathFame": 0,
      "KillFame": 169209,
      "FameRatio": 1692089.97,
      "LifetimeStatistics": {
        "PvE": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0,
          "Hellgate": 0,
          "CorruptedDungeon": 0
        },
        "Gathering": {
          "Fiber": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Hide": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Ore": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Rock": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Wood": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "All": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          }
        },
        "Crafting": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "CrystalLeague": 0,
        "FishingFame": 0,
        "FarmingFame": 0,
        "Timestamp": null
      }
    }
  ],
  "GvGMatch": null,
  "BattleId": 585250100,
  "KillArea": "OPEN_WORLD",
  "Category": null,
  "Type": "KILL"
}`,
    `{
  "groupMemberCount": 1,
  "numberOfParticipants": 1,
  "EventId": 585533051,
  "TimeStamp": "2022-09-13T09:08:46.663375500Z",
  "Version": 4,
  "Killer": {
    "AverageItemPower": 1495.17493,
    "Equipment": {
      "MainHand": {
        "Type": "T8_MAIN_1HCROSSBOW@1",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "OffHand": {
        "Type": "T6_OFF_SHIELD_AVALON@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Head": {
        "Type": "T6_HEAD_CLOTH_MORGANA@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Armor": {
        "Type": "T6_ARMOR_CLOTH_MORGANA@3",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Shoes": {
        "Type": "T8_SHOES_PLATE_SET1@1",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Bag": {
        "Type": "T6_BAG@1",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Cape": {
        "Type": "T5_CAPEITEM_FW_THETFORD@3",
        "Count": 1,
        "Quality": 5,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Mount": {
        "Type": "T4_MOUNT_GIANTSTAG",
        "Count": 1,
        "Quality": 4,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Potion": {
        "Type": "T6_POTION_HEAL@1",
        "Count": 2,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Food": {
        "Type": "T5_MEAL_SOUP",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      }
    },
    "Inventory": [],
    "Name": "Matvey1337",
    "Id": "Hl3-3Vk2R8K0u2_e6NXI_A",
    "GuildName": "Sex and Flex",
    "GuildId": "liqBII9OQ7OpQ0YUAAMLzA",
    "AllianceName": "BaDc",
    "AllianceId": "p-I26m4US5iIc6zKAMhr7A",
    "AllianceTag": "",
    "Avatar": "GVGSEASON_14_GOLD",
    "AvatarRing": "AVATARRING_ADC_TOKENLOCKED_01",
    "DeathFame": 0,
    "KillFame": 303350,
    "FameRatio": 3033499.95,
    "LifetimeStatistics": {
      "PvE": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0,
        "Hellgate": 0,
        "CorruptedDungeon": 0
      },
      "Gathering": {
        "Fiber": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Hide": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Ore": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Rock": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Wood": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "All": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        }
      },
      "Crafting": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0
      },
      "CrystalLeague": 0,
      "FishingFame": 0,
      "FarmingFame": 0,
      "Timestamp": null
    }
  },
  "Victim": {
    "AverageItemPower": 1356.16333,
    "Equipment": {
      "MainHand": {
        "Type": "T6_2H_KNUCKLES_AVALON@2",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "OffHand": null,
      "Head": {
        "Type": "T6_HEAD_CLOTH_SET3@2",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Armor": {
        "Type": "T6_ARMOR_LEATHER_SET3@2",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Shoes": {
        "Type": "T6_SHOES_PLATE_HELL@2",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Bag": {
        "Type": "T4_BAG",
        "Count": 1,
        "Quality": 2,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Cape": {
        "Type": "T5_CAPEITEM_FW_THETFORD@3",
        "Count": 1,
        "Quality": 3,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Mount": {
        "Type": "T3_MOUNT_HORSE",
        "Count": 1,
        "Quality": 1,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Potion": {
        "Type": "T6_POTION_HEAL",
        "Count": 3,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      "Food": null
    },
    "Inventory": [
      {
        "Type": "T4_POTION_COOLDOWN",
        "Count": 9,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_RUNE",
        "Count": 6,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_SOUL",
        "Count": 2,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T1_SILVERBAG_NONTRADABLE",
        "Count": 4,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T4_RELIC",
        "Count": 2,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_SOUL",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T5_RUNE",
        "Count": 4,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      {
        "Type": "T6_RUNE",
        "Count": 1,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      {
        "Type": "T1_SKILLBOOK_NONTRADABLE",
        "Count": 55,
        "Quality": 0,
        "ActiveSpells": [],
        "PassiveSpells": []
      },
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null
    ],
    "Name": "OhTuDou",
    "Id": "JX2Zi75jSx2Xrx70fciWpA",
    "GuildName": "Breakfast-Club",
    "GuildId": "2QiOHXMpTDyhPapy_DcBLQ",
    "AllianceName": "",
    "AllianceId": "",
    "AllianceTag": "",
    "Avatar": "GVGSEASON_16_SILVER",
    "AvatarRing": "RING_GVGSEASONREWARD_SILVER",
    "DeathFame": 303350,
    "KillFame": 0,
    "FameRatio": 0,
    "LifetimeStatistics": {
      "PvE": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0,
        "Hellgate": 0,
        "CorruptedDungeon": 0
      },
      "Gathering": {
        "Fiber": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Hide": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Ore": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Rock": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "Wood": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "All": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        }
      },
      "Crafting": {
        "Total": 0,
        "Royal": 0,
        "Outlands": 0,
        "Avalon": 0
      },
      "CrystalLeague": 0,
      "FishingFame": 0,
      "FarmingFame": 0,
      "Timestamp": null
    }
  },
  "TotalVictimKillFame": 303350,
  "Location": null,
  "Participants": [
    {
      "AverageItemPower": 1495.17493,
      "Equipment": {
        "MainHand": {
          "Type": "T8_MAIN_1HCROSSBOW@1",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "OffHand": {
          "Type": "T6_OFF_SHIELD_AVALON@3",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Head": {
          "Type": "T6_HEAD_CLOTH_MORGANA@3",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Armor": {
          "Type": "T6_ARMOR_CLOTH_MORGANA@3",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Shoes": {
          "Type": "T8_SHOES_PLATE_SET1@1",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Bag": {
          "Type": "T6_BAG@1",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Cape": {
          "Type": "T5_CAPEITEM_FW_THETFORD@3",
          "Count": 1,
          "Quality": 5,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Mount": {
          "Type": "T4_MOUNT_GIANTSTAG",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Potion": {
          "Type": "T6_POTION_HEAL@1",
          "Count": 2,
          "Quality": 0,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "Food": {
          "Type": "T5_MEAL_SOUP",
          "Count": 1,
          "Quality": 0,
          "ActiveSpells": [],
          "PassiveSpells": []
        }
      },
      "Inventory": [],
      "Name": "Matvey1337",
      "Id": "Hl3-3Vk2R8K0u2_e6NXI_A",
      "GuildName": "Sex and Flex",
      "GuildId": "liqBII9OQ7OpQ0YUAAMLzA",
      "AllianceName": "BaDc",
      "AllianceId": "p-I26m4US5iIc6zKAMhr7A",
      "AllianceTag": "",
      "Avatar": "GVGSEASON_14_GOLD",
      "AvatarRing": "AVATARRING_ADC_TOKENLOCKED_01",
      "DeathFame": 0,
      "KillFame": 0,
      "FameRatio": 0,
      "LifetimeStatistics": {
        "PvE": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0,
          "Hellgate": 0,
          "CorruptedDungeon": 0
        },
        "Gathering": {
          "Fiber": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Hide": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Ore": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Rock": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Wood": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "All": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          }
        },
        "Crafting": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "CrystalLeague": 0,
        "FishingFame": 0,
        "FarmingFame": 0,
        "Timestamp": null
      },
      "DamageDone": 7717,
      "SupportHealingDone": 0
    }
  ],
  "GroupMembers": [
    {
      "AverageItemPower": 0,
      "Equipment": {
        "MainHand": {
          "Type": "T8_MAIN_1HCROSSBOW@1",
          "Count": 1,
          "Quality": 4,
          "ActiveSpells": [],
          "PassiveSpells": []
        },
        "OffHand": null,
        "Head": null,
        "Armor": null,
        "Shoes": null,
        "Bag": null,
        "Cape": null,
        "Mount": null,
        "Potion": null,
        "Food": null
      },
      "Inventory": [],
      "Name": "Matvey1337",
      "Id": "Hl3-3Vk2R8K0u2_e6NXI_A",
      "GuildName": "Sex and Flex",
      "GuildId": "liqBII9OQ7OpQ0YUAAMLzA",
      "AllianceName": "BaDc",
      "AllianceId": "p-I26m4US5iIc6zKAMhr7A",
      "AllianceTag": "",
      "Avatar": "GVGSEASON_14_GOLD",
      "AvatarRing": "AVATARRING_ADC_TOKENLOCKED_01",
      "DeathFame": 0,
      "KillFame": 303350,
      "FameRatio": 3033499.95,
      "LifetimeStatistics": {
        "PvE": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0,
          "Hellgate": 0,
          "CorruptedDungeon": 0
        },
        "Gathering": {
          "Fiber": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Hide": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Ore": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Rock": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "Wood": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          },
          "All": {
            "Total": 0,
            "Royal": 0,
            "Outlands": 0,
            "Avalon": 0
          }
        },
        "Crafting": {
          "Total": 0,
          "Royal": 0,
          "Outlands": 0,
          "Avalon": 0
        },
        "CrystalLeague": 0,
        "FishingFame": 0,
        "FarmingFame": 0,
        "Timestamp": null
      }
    }
  ],
  "GvGMatch": null,
  "BattleId": 585533051,
  "KillArea": "OPEN_WORLD",
  "Category": null,
  "Type": "KILL"
}`,
}

package main

import (
	"albion-event-fetcher/internal/models"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
)

var db *sql.DB

func main() {
	var err error
	db, err = sql.Open("mysql", fmt.Sprintf("%s@/%s", "albionapp", "albion"))
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	for convertBatch() > 0 {
	}
}

func convertBatch() int {
	rows, err := db.Query("select id, main_hand_item, main_hand_tier, main_hand_enchant, main_hand_quality, off_hand_item, off_hand_tier, off_hand_enchant, off_hand_quality, head_item, head_tier, head_enchant, head_quality, body_item, body_tier, body_enchant, body_quality, shoe_item, shoe_tier, shoe_enchant, shoe_quality, bag_item, bag_tier, bag_enchant, bag_quality, cape_item, cape_tier, cape_enchant, cape_quality, mount_item, mount_tier, mount_quality, hash from loadouts where hash is null limit 1000")
	if err != nil {
		panic(err)
	}

	loadouts := make([]models.Loadout, 0)

	for rows.Next() {
		l := models.Loadout{}
		err = rows.Scan(
			&l.Id,
			&l.MainHandItem,
			&l.MainHandTier,
			&l.MainHandEnchant,
			&l.MainHandQuality,
			&l.OffHandItem,
			&l.OffHandTier,
			&l.OffHandEnchant,
			&l.OffHandQuality,
			&l.HeadItem,
			&l.HeadTier,
			&l.HeadEnchant,
			&l.HeadQuality,
			&l.BodyItem,
			&l.BodyTier,
			&l.BodyEnchant,
			&l.BodyQuality,
			&l.ShoeItem,
			&l.ShoeTier,
			&l.ShoeEnchant,
			&l.ShoeQuality,
			&l.BagItem,
			&l.BagTier,
			&l.BagEnchant,
			&l.BagQuality,
			&l.CapeItem,
			&l.CapeTier,
			&l.CapeEnchant,
			&l.CapeQuality,
			&l.MountItem,
			&l.MountTier,
			&l.MountQuality,
			&l.Hash,
		)

		if err != nil {
			panic(err)
		}

		l.GenerateId()
		loadouts = append(loadouts, l)
	}

	tx, err := db.Begin()
	if err != nil {
		panic(err)
	}

	for _, l := range loadouts {
		_, err := tx.Exec("update loadouts set hash = ? where id = ?", l.Hash, l.Id)
		if err != nil {
			tx.Rollback()
			panic(err)
		}
	}

	err = tx.Commit()
	if err != nil {
		panic(err)
	}

	log.Printf("Converted %d loadouts\n", len(loadouts))

	return len(loadouts)
}

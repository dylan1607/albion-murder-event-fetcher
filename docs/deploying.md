## How to deploy a new version

There are two services in this repo, their containers are defined by
`Dockerfile.fetcher` and `Dockerfile.persister`, and the k8s specifications for
both are in `./k8s`. They are both versioned and deployed together. This means
even if you only make changes to the fetcher, a new version of the persister
will still be built and deployed.

### 1. Pick Next Version Number
This project roughly follows semver, but since it's an application the rules are
not strict. Use your best judgement.

### 2. Create Tag
Create a new version tag on the master branch using the selected version:
```
$ git tag v1.1.1
```

### 3. Push Tag
```
# git push origin v1.1.1
```

### 4. Deploy
Manually trigger the "deploy" pipeline on the tag in gitlab.

Once the deployment job is triggered, monitor the k8s cluster and logs with k9s
or the DO k8s dashboard.

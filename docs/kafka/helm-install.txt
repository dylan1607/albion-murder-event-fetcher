Deploying: https://artifacthub.io/packages/helm/bitnami/kafka

logRetentionBytes = 10000000 (10GB, default is 1GB)
persistence.size = 15Gi (default is 8Gi)

Command:
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install murderledger-kafka-release bitnami/kafka --namespace albion --set logRetentionBytes=10000000,persistence.size=15Gi

------------ Install Log ------------
❯ helm install murderledger-kafka-release bitnami/kafka --namespace albion --set logRetentionBytes=_10000000,persistence.size=15Gi,logRetentionHours=168
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/david/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/david/.kube/config
NAME: murderledger-kafka-release
LAST DEPLOYED: Thu Sep 16 21:43:08 2021
NAMESPACE: albion
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
** Please be patient while the chart is being deployed **

Kafka can be accessed by consumers via port 9092 on the following DNS name from within your cluster:

    murderledger-kafka-release.albion.svc.cluster.local

Each Kafka broker can be accessed by producers via port 9092 on the following DNS name(s) from within your cluster:

    murderledger-kafka-release-0.murderledger-kafka-release-headless.albion.svc.cluster.local:9092

To create a pod that you can use as a Kafka client run the following commands:

    kubectl run murderledger-kafka-release-client --restart='Never' --image docker.io/bitnami/kafka:2.8.0-debian-10-r84 --namespace albion --command -- sleep infinity
    kubectl exec --tty -i murderledger-kafka-release-client --namespace albion -- bash

    PRODUCER:
        kafka-console-producer.sh \

            --broker-list murderledger-kafka-release-0.murderledger-kafka-release-headless.albion.svc.cluster.local:9092 \
            --topic test

    CONSUMER:
        kafka-console-consumer.sh \

            --bootstrap-server murderledger-kafka-release.albion.svc.cluster.local:9092 \
            --topic test \
            --from-beginning

------------ 2022-01-17 Update Log ------------
Used this comment to create a values file with the current settings
```
$ helm get values -n albion murderledger-kafka-release -o yaml > values.yaml
```

Added (default was 1MB)
```yaml
maxMessageBytes: _2000012
```

Run upgrade command:
```
$ helm upgrade -f values.yaml -n albion murderledger-kafka-release bitnami/kafka
```

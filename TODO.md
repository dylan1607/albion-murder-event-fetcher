## Todo

## Completed
- [x] Make fetcher decode event json and keep in-memory list of recent events
        to keep tons of dupicate data out of Kafka logs
- [x] Move k8s-new/ to replace k8s/
- [x] Delete raw-events topic from Kafka (replaced by just 'events')
- [x] Replace fetcher with newfetcher
- [x] Deploy to geared k8s cluster
- [x] Setup gitlab build
- [x] Dockerize
- [x] Setup logging with sentry
- [x] Use .env configs
- [x] Check http status of API response and log accordingly
- [x] Setup on k8s or droplet
- [x] Add a README
- [x] Fix loadout hashing (need to convert existing IDs)
- [x] Clean up error handling
- [x] Create repo and organize project

module albion-event-fetcher

go 1.15

require (
	github.com/getsentry/sentry-go v0.11.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/hashicorp/golang-lru v0.5.4
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.3.0
	github.com/pkg/errors v0.8.1
	github.com/segmentio/kafka-go v0.4.19
)
